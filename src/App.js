import React, { useEffect, useState } from 'react';
import { api } from './api';
import { removeColumnFromStorage } from './api/api-storage-service';
import { Column, Header } from './components';

function App() {
  //Объявим state для хранения юзера
  // useState();
  const [user, setUser] = useState('');
  //Объявим state для хранения колонок
  // useState();
  const [columns, setColumns] = useState([]);
  const [columnText, setColumnText] = useState('');

  const [isAddColumnLoading, setIsAddColumnLoading] = useState(false);

  /**
   * Функция, которая делает запрос к API и создает новую колонку
   * (!) Обратите внимание, что мы храним
   *  в isAddColumnLoading статус выполнения запроса: загружается он или нет
   */
  const handleCreateColumn = async () => {
    setIsAddColumnLoading(true);

    /**
     * Здесь вызываем метод API на создание колонки
     * (!) После успешного создания колонки через API
     *  мы должны добавить ее в локальное состояние хранящее колонки,
     *  чтобы отобразить ее
     */
    const column = await api.createColumn(columnText);
    /**
     * (?) Возможно, здесь еще стоит сбрасывать состояние инпута для имени
     *  новой колонки.
     */

    setColumns([...columns, column]);
    setColumnText('')

    setIsAddColumnLoading(false);
  };

  /**
   * Метод для удаления карточки.
   * Он должен делать запрос к API
   *  и затем удалять карточку из локального состояния
   * @param {number} columnId
   */
  const handleDeleteColumn = async (columnId) => {
    /**
     * 1. API call
     * 2. Change columns state
     */
    await api.deleteColumn(columnId);
    setColumns(columns.filter(column => column.id !== columnId))
  };

  /**
   * Метод для создания карточки в колонке с ID columnId.
   * Делает запрос к API и добавляет карточку в локальное состояние
   * @param {number} columnId
   * @param {number} cardName
   */
  const handleCreateCard = async (columnId, cardName) => {
    /**
     * 1. API call
     * 2. Change cards state
     */
     const card = await api.createCard(columnId, cardName);
     setColumns(columns.map(column => {
      if (column.id === columnId) {column.cards.push(card)}
      return column }))

  };

  /**
   * Метод для удаления карточки с ID cardId
   * Делает запрос к API и удаляет карточку и локального состояния
   * @param {number} cardId
   */
  const handleDeleteCard = async (cardId) => {
    /**
     * 1. API call
     * 2. Change cards state
     */
     await api.deleteCard(cardId);
     setColumns(columns.map(column => {
      if (column.cards.find(card => cardId === card.id)){
        return {...column, cards: column.cards.filter(card => cardId !== card.id)}
      }
      return column
    }))

  };

  /**
   * Функция подгрузки колонок и юзера -- начального состояния приложения.
   * Она должна запрашивать юзера и колонки с API, класть их в локальное состояние
   * */
  const fetchState = async () => {
    /**
     * 1. API call
     * 2. Change columns state
     * 3. Change user state
     */
     const beginUser = await api.getCurrentUser();
     const beginColumns = await api.getColumns();

     setColumns(beginColumns);
     setUser(beginUser.name);
  };

  /**
   * Task: На useEffect-е мы должны вызывать функцию fetchState.
   * Эта функция должна запрашивать с API: текущего юзера и все колонки
   */

  // useEffect()
  useEffect(() => {fetchState()}, []);

  const renderColumns = () => {
    //Если колонки грузятся, то покажем загрузку
    if (!columns) {
      return <div>Loading...</div>;
    }

    //Если их нету -- пустой экран
    if (columns.length === 0) {
      return (
        <div>
          Nothing to show :( <br /> Create first column!
        </div>
      );
    }

    //Иначе -- отобразим все колонки
    return columns.map((column) => {
      return (
        <Column
          //Пропишем все пропсы, которые хочет Column (см. компонент Column)
          key={column.id}
          id={column.id}
          name={column.name}
          cards={column.cards}
          onDelete={handleDeleteColumn}
          onAddCard={handleCreateCard}
          onDeleteCard={handleDeleteCard}
        />
      );
    });
  };

  return (
    <div>
      <Header name={user} />
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          marginBottom: '24px',
        }}
      >
        {renderColumns()}
      </div>
      <div>
        <form>
          <input
            //! Не трогайте строку ниже -- она нужна для тестов !
            id="create_column_input"
            placeholder="Column name"
            // Нужно хранить value-инпута в локальном состоянии
            value={columnText}
            // И менять его на новое введенное значение на событии onChange
            // (!) новое введенное значение в подписчике можно получить так:
            // (event) => event.target.value (<-- tadaaa 🎉)
            onChange={(event) => {setColumnText(event.target.value)}}
          />
          <button
            //! Не трогайте строку ниже -- она нужна для тестов !
            id="create_column_button"
            type="button"
            // По клику -- создаем колонку!
            onClick={handleCreateColumn}
            // Пока создание колонки "загружается" переводим колонку в disabled
            disabled={isAddColumnLoading}
          >
            Create
          </button>
        </form>
      </div>
    </div>
  );
}

export default App;
